import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AddStoryComponent } from './components/add-story/add-story.component';
import { SprintPointComponent } from './components/sprint-point/sprint-point.component';
import { SprintCalculatorComponent } from './components/sprint-calculator/sprint-calculator.component';
import { SelectedSprintComponent } from './components/selected-sprint/selected-sprint.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './components/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    AddStoryComponent,
    SprintPointComponent,
    SprintCalculatorComponent,
    SelectedSprintComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
