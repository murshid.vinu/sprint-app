import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-sprint-point',
  templateUrl: './sprint-point.component.html',
  styleUrls: ['./sprint-point.component.css']
})
export class SprintPointComponent implements OnInit {

  @Input() stories: any = [];

  constructor() { }

  ngOnInit(): void {
  }

}
