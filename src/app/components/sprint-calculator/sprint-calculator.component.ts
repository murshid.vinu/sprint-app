import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-sprint-calculator',
  templateUrl: './sprint-calculator.component.html',
  styleUrls: ['./sprint-calculator.component.css']
})
export class SprintCalculatorComponent implements OnInit {

  @Output() eventItem = new EventEmitter();

  formGroup: FormGroup;

  constructor(private fb: FormBuilder) {
    this.formGroup = this.fb.group({
      points: ['', Validators.required]
    })
  }

  get form() {
    return this.formGroup.controls;
  }

  ngOnInit(): void {
  }

  submitForm() {
    this.formGroup.markAllAsTouched();
    if (this.formGroup.invalid) {
      return;
    }
    this.eventItem.emit({ type: 'calculate', point: this.formGroup.value?.points });
  }

  clear(type: string) {
    if (type == 'all') {
      this.eventItem.emit({ type: 'stories' });
    } else {
      this.eventItem.emit({ type: 'selected_stories' });
    }
  }

}
