import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedSprintComponent } from './selected-sprint.component';

describe('SelectedSprintComponent', () => {
  let component: SelectedSprintComponent;
  let fixture: ComponentFixture<SelectedSprintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectedSprintComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SelectedSprintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
