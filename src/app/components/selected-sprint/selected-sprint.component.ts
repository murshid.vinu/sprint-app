import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-selected-sprint',
  templateUrl: './selected-sprint.component.html',
  styleUrls: ['./selected-sprint.component.css']
})
export class SelectedSprintComponent implements OnInit {

  @Input() selectedStories: any = [];

  constructor() { }

  ngOnInit(): void {
  }

}
