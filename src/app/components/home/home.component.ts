import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  stories: any = [];
  selectedStories: any = [];

  constructor() {}

  ngOnInit(): void {
  }

  addStory(event: any) {
    if (this.stories.find((story: any) => story.points === event.points)) {
      alert('The point already exists');
    } else {
      this.stories.push(event);
    }
  }

  getSelectedStories(storiesTemp: any = []) {
    const resposeArray: any = [[]];
    for (let i = 0; i < storiesTemp.length; i++) {
      const responseLength = resposeArray.length
      for (let j = 0; j < responseLength; j++) {
        const subArray: any = resposeArray[j].slice();
        subArray.push(storiesTemp[i]);
        resposeArray.push(subArray.slice());
      }
    }
    return resposeArray;
  }

  addSprintPoint(event: any) {
    switch (event.type) {
      case 'calculate':
        let sortedStories = [...this.stories];
        sortedStories.sort((a: any, b: any) => b.points - a.points);
        const resposeArray = this.getSelectedStories(sortedStories.map(story => story.points));
        const filteredArray = resposeArray.filter((res: any) => event.point === res.reduce((acc: any, crr: any) => {
          acc+=crr;
          return acc
        }, 0));
        if (filteredArray.length) {
          this.selectedStories = this.stories.filter((story: any) => filteredArray[0].includes(story.points))
        }
        break;
      case 'stories':
        this.stories = [];
        this.selectedStories = [];
        break;
      case 'selected_stories':
        this.selectedStories = [];
        break;
      default:
        break;
    }
  }

}
